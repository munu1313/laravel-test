<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profiles extends Model
{
    protected $fillable = [
        'name', 'address', 'status', 'pdf',
    ];
}
