@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">
                        Profiles
                    </h3>

                    <a href="/addprofile" class="btn btn-success pull-right">
                        <span class="glyphicon glyphicon-plus"></span> Add </a>
                    <div class="clearfix"></div>
                </div>

                {{--
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif You are logged in!
                </div> --}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget
                        metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p>
                        <a href="#" class="btn btn-primary" role="button">
                            <i class="glyphicon glyphicon-pencil"></i>Button</a>
                        <a href="#" class="btn btn-default" role="button">Button</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection